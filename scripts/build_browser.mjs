import { buildBrowser } from './utils/browser_jobs.mjs';

async function main() {
  await buildBrowser();
}

main();
